package de.hsb.softw2muebase;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
public class HelloController {

    @GetMapping("/greeting/{name}")
    public String greeting(@PathVariable("name") String name) {
        if (name.length() % 2 == 0) {
            name = "wuensche ich Ihnen, " + name;
        }
        return "Guten Tag " + name;
    }
}
